package ru.t1.azarin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.dto.model.TaskDto;
import ru.t1.azarin.tm.dto.request.task.TaskListRequest;
import ru.t1.azarin.tm.enumerated.Sort;
import ru.t1.azarin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "task-list";

    @NotNull
    public final static String DESCRIPTION = "Show all tasks.";

    @Override
    public void execute() {
        System.out.println("[ALL TASKS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setSort(sort);
        @NotNull final List<TaskDto> tasks = taskEndpoint.listTaskResponse(request).getTasks();
        renderTask(tasks);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
