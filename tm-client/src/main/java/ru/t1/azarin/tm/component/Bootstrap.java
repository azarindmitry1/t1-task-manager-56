package ru.t1.azarin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.api.service.ICommandService;
import ru.t1.azarin.tm.api.service.ILoggerService;
import ru.t1.azarin.tm.command.AbstractCommand;
import ru.t1.azarin.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.azarin.tm.exception.system.CommandNotSupportedException;
import ru.t1.azarin.tm.util.SystemUtil;
import ru.t1.azarin.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;

@Getter
@Component
public final class Bootstrap {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.azarin.tm.command";

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    private boolean processArguments(@Nullable final String[] args) throws SQLException {
        if (args == null || args.length == 0) return false;
        final String param = args[0];
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(param);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
        return true;
    }

    public void processCommands(@Nullable final String command) throws SQLException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        abstractCommand.execute();
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    @SneakyThrows
    public void initCommands(@Nullable final AbstractCommand[] commands) {
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) return;
            commandService.add(command);
        }
    }

    private void prepareStartup() {
        initPid();
        initCommands(abstractCommands);
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    public void run(@Nullable final String[] args) throws SQLException {
        if (processArguments(args)) System.exit(0);

        prepareStartup();

        while (true) {
            try {
                System.out.println("ENTER THE COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommands(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[ERROR]");
            }
        }
    }

}