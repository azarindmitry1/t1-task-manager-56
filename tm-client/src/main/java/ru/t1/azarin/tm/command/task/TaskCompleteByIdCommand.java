package ru.t1.azarin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.dto.request.task.TaskCompleteByIdRequest;
import ru.t1.azarin.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "task-complete-by-id";

    @NotNull
    public final static String DESCRIPTION = "Complete task by id.";

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(getToken());
        request.setId(id);
        taskEndpoint.completeTaskByIdResponse(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
