package ru.t1.azarin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.api.endpoint.ISystemEndpoint;
import ru.t1.azarin.tm.api.service.ICommandService;
import ru.t1.azarin.tm.command.AbstractCommand;
import ru.t1.azarin.tm.enumerated.Role;

@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    protected ICommandService commandService;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
