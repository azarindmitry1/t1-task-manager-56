package ru.t1.azarin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.azarin.tm.util.TerminalUtil;

@Component
public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "project-create";

    @NotNull
    public final static String DESCRIPTION = "Create new project.";

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        projectEndpoint.createResponse(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
