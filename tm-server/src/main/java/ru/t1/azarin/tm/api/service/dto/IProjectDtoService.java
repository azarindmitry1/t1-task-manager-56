package ru.t1.azarin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.ProjectDto;
import ru.t1.azarin.tm.enumerated.Status;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDto> {

    void changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    ProjectDto create(@Nullable String userId, @Nullable String name);

    @NotNull
    ProjectDto create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

}