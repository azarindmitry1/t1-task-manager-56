package ru.t1.azarin.tm.api.repository.dto;

import ru.t1.azarin.tm.dto.model.SessionDto;

public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDto> {
}
