package ru.t1.azarin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.api.service.dto.IUserDtoService;
import ru.t1.azarin.tm.dto.model.UserDto;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.entity.UserNotFoundException;
import ru.t1.azarin.tm.exception.field.EmailEmptyException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.LoginEmptyException;
import ru.t1.azarin.tm.exception.field.PasswordEmptyException;
import ru.t1.azarin.tm.exception.user.ExistEmailException;
import ru.t1.azarin.tm.exception.user.ExistLoginException;
import ru.t1.azarin.tm.exception.user.RoleEmptyException;
import ru.t1.azarin.tm.repository.dto.UserDtoRepository;
import ru.t1.azarin.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@NoArgsConstructor
public final class UserDtoService extends AbstractDtoService<UserDto, IUserDtoRepository> implements IUserDtoService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    public IUserDtoRepository getRepository() {
        return context.getBean(UserDtoRepository.class);
    }

    @Override
    public void add(@Nullable final UserDto model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public UserDto create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDto user = new UserDto();
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        try {
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDto create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistEmailException();
        @Nullable final UserDto user = new UserDto();
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(Role.USUAL);
        try {
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDto create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @Nullable final UserDto user = new UserDto();
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        try {
            entityManager.getTransaction().begin();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    public List<UserDto> findAll() {
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDto findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findByLogin(login);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDto findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findByEmail(email);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public @Nullable UserDto findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findByLogin(login) != null;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            return userRepository.findByEmail(email) != null;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final UserDto model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        remove(user);
    }

    @Override
    public void removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDto user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        remove(user);
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDto user;
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            user = userRepository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDto user;
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            user = userRepository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDto user;
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            user = userRepository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable final UserDto model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            userRepository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDto user;
        @NotNull final IUserDtoRepository userRepository = getRepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            user = userRepository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setFirstName(firstName);
            user.setMiddleName(middleName);
            user.setLastName(lastName);
            userRepository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
