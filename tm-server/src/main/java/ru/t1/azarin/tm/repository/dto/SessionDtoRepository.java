package ru.t1.azarin.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.azarin.tm.dto.model.SessionDto;

import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDto> implements ISessionDtoRepository {

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM SessionDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<SessionDto> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, SessionDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDto findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, SessionDto.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "DELETE FROM SessionDTO m WHERE m.userId = :userId AND m.id = :id";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

}
