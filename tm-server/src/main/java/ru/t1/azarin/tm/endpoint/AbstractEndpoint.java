package ru.t1.azarin.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.azarin.tm.api.service.IAuthService;
import ru.t1.azarin.tm.dto.model.SessionDto;
import ru.t1.azarin.tm.dto.request.user.AbstractUserRequest;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.exception.user.AccessDeniedException;

@Controller
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    private IAuthService authService;

    protected SessionDto check(@NotNull final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull final SessionDto session = authService.validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    protected SessionDto check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return authService.validateToken(token);
    }

}
