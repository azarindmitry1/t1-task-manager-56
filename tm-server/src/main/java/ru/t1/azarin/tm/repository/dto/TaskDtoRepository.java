package ru.t1.azarin.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.azarin.tm.dto.model.TaskDto;
import ru.t1.azarin.tm.enumerated.Sort;

import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDto> implements ITaskDtoRepository {

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM TaskDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<TaskDto> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<TaskDto> findAll(@NotNull final String userId, @NotNull final Sort sort) {
        @NotNull final String jpql = String.format("SELECT m FROM TaskDTO m WHERE m.userId = :userId ORDER_BY m.%s",
                getSortColumn(sort.getComparator()));
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<TaskDto> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDto findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, TaskDto.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "DELETE FROM TaskDTO m WHERE m.userId = :userId AND m.id = :id";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

}
