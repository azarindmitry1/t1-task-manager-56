package ru.t1.azarin.tm.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.dto.model.ProjectDto;
import ru.t1.azarin.tm.dto.model.SessionDto;
import ru.t1.azarin.tm.dto.model.TaskDto;
import ru.t1.azarin.tm.dto.model.UserDto;
import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.model.Session;
import ru.t1.azarin.tm.model.Task;
import ru.t1.azarin.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("ru.t1.azarin.tm")
public class ServerConfiguration {

    @NotNull
    @Autowired
    public IPropertyService propertyService;

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, propertyService.getDatabaseUrl());
        settings.put(org.hibernate.cfg.Environment.USER, propertyService.getDatabaseUser());
        settings.put(org.hibernate.cfg.Environment.PASS, propertyService.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, propertyService.getDatabaseDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2ddlAuto());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, propertyService.getDatabaseShowSql());
        settings.put(Environment.FORMAT_SQL, propertyService.getDatabaseFormatSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDatabaseSecondLvlCache());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getDatabaseFactoryClass());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getDatabaseUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getDatabaseUseMinPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getDatabaseRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDatabaseConfigFilePath());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(ProjectDto.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(TaskDto.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(UserDto.class);
        source.addAnnotatedClass(Session.class);
        source.addAnnotatedClass(SessionDto.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager() {
        return entityManagerFactory().createEntityManager();
    }

}
