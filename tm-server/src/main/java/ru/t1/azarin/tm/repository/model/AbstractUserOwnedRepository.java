package ru.t1.azarin.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.azarin.tm.comparator.CreatedComparator;
import ru.t1.azarin.tm.comparator.NameComparator;
import ru.t1.azarin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;

@Repository
@Scope("prototype")
@NoArgsConstructor
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    @NotNull
    protected String getSortColumn(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == NameComparator.INSTANCE) return "name";
        else return "status";
    }

}
