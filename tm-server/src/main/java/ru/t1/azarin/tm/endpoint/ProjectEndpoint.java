package ru.t1.azarin.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.azarin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.azarin.tm.api.service.dto.IProjectDtoService;
import ru.t1.azarin.tm.dto.model.ProjectDto;
import ru.t1.azarin.tm.dto.model.SessionDto;
import ru.t1.azarin.tm.dto.request.project.*;
import ru.t1.azarin.tm.dto.response.project.*;
import ru.t1.azarin.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.azarin.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectDtoService projectServiceDTO;

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeStatusByIdResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        projectServiceDTO.changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clearResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) {
        @NotNull final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        projectServiceDTO.clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIdResponse completeByIdResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIdRequest request
    ) {
        @NotNull final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        projectServiceDTO.changeProjectStatusById(userId, id, Status.COMPLETED);
        return new ProjectCompleteByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse createResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) {
        @NotNull final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDto project = projectServiceDTO.create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListResponse listResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectListRequest request
    ) {
        @NotNull final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final List<ProjectDto> projects = projectServiceDTO.findAll(userId);
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeByIdResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        @NotNull final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        projectServiceDTO.removeById(userId, id);
        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectShowByIdResponse showByIdResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIdRequest request
    ) {
        @NotNull final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final ProjectDto project = projectServiceDTO.findOneById(userId, id);
        return new ProjectShowByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIdResponse startByIdResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIdRequest request
    ) {
        @NotNull final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        projectServiceDTO.changeProjectStatusById(userId, id, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateByIdResponse(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        @NotNull final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        projectServiceDTO.updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse();
    }

}
