package ru.t1.azarin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.ProjectDto;
import ru.t1.azarin.tm.enumerated.Sort;

import java.util.List;

public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDto> {

    @Nullable
    List<ProjectDto> findAll(@NotNull String userId, @NotNull Sort sort);

}