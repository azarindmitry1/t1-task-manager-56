package ru.t1.azarin.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.enumerated.Sort;
import ru.t1.azarin.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    void clear(@Nullable String userId);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<M> findAll(@Nullable String userId);

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    void removeById(@Nullable String userId, @Nullable String id);

}
